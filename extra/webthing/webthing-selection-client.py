#!/usr/bin/env python3
# -*- mode: python; python-indent-offset: 4; indent-tabs-mode: nil -*-
# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Indentifier: Apache-2.0
# Contact: Philippe Coval <philippe.coval@huawei.com>

# Usage:
# pip3 install --user websocket-client

import sys
import websocket

try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("on_closed")

def on_open(ws):
    def run(*args):
        time.sleep(1)
        ws.send('{"messageType": "setProperty", "data": {"selection": [0, 0, 0, 1]}}')
        time.sleep(1)
        ws.send('{"messageType": "requestAction", "data": { "deliver": {"input": {"selection": [0, 1, 0, 0]}}}}')
        print("thread terminating...")
        ws.send('{"messageType": "addEventSubscription", "data": { "delivered": [] }}')
    thread.start_new_thread(run, ())


if __name__ == "__main__":
    url = "ws://localhost:8888/" if (len(sys.argv) <= 1) else str(sys.argv[1])
    ws = websocket.WebSocketApp(url,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
