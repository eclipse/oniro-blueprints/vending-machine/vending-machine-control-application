#!/usr/bin/more
# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Indentifier: Apache-2.0

Those python programs can help to check interoperability,
they are using webthing framework and support HTTP and WebSockets.

Current project only need WebSockets support,
also note that this payload's format is enriched 
compared to what it currently specified
however communication should be compatible.

For the record it works this way:

````    
    pip3 install --user websocket-client webthing
    ./webthing-selection-server.py
    
    $ ./webthing-selection-client.py
    {"messageType": "propertyStatus", "data": {"selection": [0, 0, 0, 1]}}
    {"messageType": "actionStatus", "data": {"deliver": {"href": "/actions/deliver/eeb354876e5b401bae1f9f7a7ba67015", "timeRequested": "2021-10-05T08:36:58+00:00", "status": "created", "input": {"selection": [0, 1, 0, 0]}}}}
    {"messageType": "actionStatus", "data": {"deliver": {"href": "/actions/deliver/eeb354876e5b401bae1f9f7a7ba67015", "timeRequested": "2021-10-05T08:36:58+00:00", "status": "pending", "input": {"selection": [0, 1, 0, 0]}}}}
    {"messageType": "event", "data": {"delivered": {"timestamp": "2021-10-05T08:36:59+00:00"}}}
    {"messageType": "actionStatus", "data": {"deliver": {"href": "/actions/deliver/eeb354876e5b401bae1f9f7a7ba67015", "timeRequested": "2021-10-05T08:36:58+00:00", "status": "completed", "input": {"selection": [0, 1, 0, 0]}, "timeCompleted": "2021-10-05T08:36:59+00:00"}}}
````
