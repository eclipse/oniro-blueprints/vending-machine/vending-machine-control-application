#  SPDX-FileCopyrightText: Huawei Inc.
#
#  SPDX-License-Identifier: Apache-2.0

.PHONY:clean

project?=vending-machine-control-application

CFLAGS+=$(shell pkg-config --cflags libwebsockets 2> /dev/null || echo "")
LDLIBS+=$(shell pkg-config --libs libwebsockets 2> /dev/null || echo "-llibwebsockets")

CFLAGS+=$(shell pkg-config --cflags json-c 2> /dev/null || echo "")
LDLIBS+=$(shell pkg-config --libs json-c 2> /dev/null || echo "-ljson-c")

LDLIBS+=-lpim435 -li2c -lsystemd

exe?=server

prefix?=/usr/local
base_bindir?=/bin
base_libdir?=/lib
bindir?=${prefix}/${base_bindir}
sysroot?=${CURDIR}/tmp/sysroot
includedir?=${prefix}/include
libdir?=${prefix}/${base_libdir}
depsdir?=tmp/deps

CFLAGS+=-I${sysroot}${includedir}/
LDLIBS+=-L${sysroot}/${libdir}

sudo?=

src?=server.c

pim435_branch?=main
pim435_url?=https://gitlab.eclipse.org/eclipse/oniro-blueprints/core/pim435
pim435_revision?=445ed623ec8d3ecbb1d566900b4ef3fb3031d689

objs?=${src:.c=.o}

all: server client

server: ${objs}
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

client: client.o
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

clean:
	rm -f *.o */*/*.o

cleanall: clean
	rm -f server client

install: ${exe}
	${sudo} install -d "${DESTDIR}/${bindir}"
	${sudo} install $< "${DESTDIR}/${bindir}/${project}"

${depsdir}/pim435/%:
	@mkdir -p ${@D}
	git clone --recursive ${pim435_url} --branch ${pim435_branch} ${@D}
	cd ${@D} && git reset --hard ${pim435_revision}

deps/pim435: ${depsdir}/pim435/Makefile
	cd ${<D} \
	  && export CFLAGS="${CFLAGS}" \
	  && make \
	  && make install DESTDIR=${sysroot}

deps: deps/pim435
	@echo ""

setup/debian: /etc/debian_version
	-${sudo} apt-get update -y
	${sudo} apt-get install -y \
		gcc \
		libjson-c-dev \
		libi2c-dev \
		libwebsockets-dev \
		make \
		pkg-config \
		sudo

/etc/debian_version:
	@echo "error: distro not supported please file a bug: ${url}"

setup: setup/debian
	@-sync
